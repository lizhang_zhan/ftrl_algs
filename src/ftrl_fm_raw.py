from __future__ import division
from collections import defaultdict
import numpy as np
from math import exp, copysign, log, sqrt

class FTRLFM(object):
    """
    Factorization Machine FTRL online Learner

    Attributes:
    ----------
    n : int, number of input features
    k : int, dim of latent factors 
    alpha : float, per-coordinate learning rate
    beta : float, per-coordinate learning rate
    l1 : float, l1 regularization for the first order weights
    l2 : float, l2 regularization for the first order weights
    alpha_2nd : float, per-ceil learning rate
    beta_2nd : float, per-eil learning rate
    l1_2nd : float, l1 regularization for the second order weights
    l2_2nd : float, l2 regularization for the second order weights
    dropout : float, dropout rate
    """
    def __init__(self, n, k, 
            alpha = 0.15, beta = 1.0, l1 = 0.01, l2 = 1.0, 
            alpha_2nd = 0.15, beta_2nd = 1.0, l1_2nd = 0.1, l2_2nd=2.0,
            dropout=0.0, seed=0):
        self.n = n
        self.k = k
        self.alpha = alpha
        self.beta = beta
        self.l1 = l1
        self.l2 = l2
        self.alpha_2nd = alpha_2nd
        self.beta_2nd = beta_2nd
        self.l1_2nd = l1_2nd
        self.l2_2nd = l2_2nd
        self.dropout = dropout
        
        rand = np.random.RandomState(seed)
        # first order model parameters
        self.grad_squ_sum = np.zeros((self.n + 1,), dtype=np.float64) # squared sum of history gradients
        self.w = np.zeros((self.n + 1,), dtype=np.float64)
        self.lazy_w = np.zeros((self.n + 1,), dtype=np.float64)
        
        # second order model parameters
        self.grad_squ_sum_2nd = np.zeros((self.n * self.k,), dtype=np.float64)
        self.w_2nd = (rand.rand(self.n * self.k) - .5) * 1e-6
        self.lazy_w_2nd = np.zeros((self.n * self.k,), dtype=np.float64)
        
    def update(self, x, grad):
        """
        Update the model with FTRL learner.

        Parameters:
        ----------
        x : a list of tuple, like (feat_id, feat_val), nozero feature index and value pair
        err: float, a error between the prediction of the model and target.

        Returns:
        ---------
        weight : array, model weight
        counts: array, model counts
        """
        grad2 = grad * grad
        # update bias
        self.w[0] += grad - self.lazy_w[0] * (sqrt(self.grad_squ_sum[0] + grad2) - sqrt(self.grad_squ_sum[0])) / self.alpha
        self.grad_squ_sum[0] += grad2
        
        # update the first order weights 
        for feat_id in x:
            sigma = (sqrt(self.grad_squ_sum[feat_id] + grad2) - sqrt(self.grad_squ_sum[feat_id])) / self.alpha
            self.w[feat_id] += grad - sigma * self.lazy_w[feat_id]
            self.grad_squ_sum[feat_id] += grad2
        
        lazy_w__2nd_sum = defaultdict(lambda: list([0]) * self.k)
        # update the second order weights
        for fi in x:
            for fj in x:
                if fi == fj:
                    continue
                for f in xrange(self.k):
                    lazy_w__2nd_sum[fi][f] += self.lazy_w_2nd[fj * self.k + f]

        for feat_id in x:
            for f in range(self.k):
                grad_2nd = grad * lazy_w__2nd_sum[feat_id][f]
                grad2_2nd = grad_2nd * grad_2nd
                sigma = (sqrt(self.grad_squ_sum_2nd[feat_id * self.k + f] + grad2_2nd) - sqrt(self.grad_squ_sum_2nd[feat_id * self.k + f])) / self.alpha_2nd
                self.w_2nd[feat_id * self.k + f] += grad_2nd - sigma * self.lazy_w_2nd[feat_id * self.k + f]
                self.grad_squ_sum_2nd[feat_id * self.k + f] += grad2_2nd
        
    def predict(self, x):
        """
        Predict probability of instance `x` with postive label

        Parameters
        ----------
        x : a list of tuple, a list of non-zero feature id and val pair

        Returns
        --------
        p : float, a predicted probability for x with postive label
        """
        z = (-self.w[0]) / ((self.beta + sqrt(self.grad_squ_sum[0])) / self.alpha) # no regularization for bias
        for feat_id in x:
            sign = -1.0 if self.w[feat_id] < 0 else 1.0
            if sign * self.w[feat_id] < self.l1:
                self.lazy_w[feat_id] = 0.0
            else:
                self.lazy_w[feat_id] = (sign * self.l1 - self.w[feat_id]) / ((self.beta + sqrt(self.grad_squ_sum[feat_id])) / self.alpha + self.l2)
            z += self.lazy_w[feat_id]
        
        #vsum = np.zeros((self.k,), dtype=np.float64)
        #v2sum = np.zeros((self.k,), dtype=np.float64)
        for feat_id in x:
            for f in range(self.k):
                sign = -1.0 if self.w_2nd[feat_id * self.k + f] < 0 else 1.0
                if sign * self.w_2nd[feat_id * self.k + f] < self.l1_2nd:
                    self.lazy_w_2nd[feat_id * self.k + f] = 0
                else:
                    self.lazy_w_2nd[feat_id * self.k + f] = (sign * self.l1_2nd - self.w_2nd[feat_id * self.k + f]) / ((self.beta_2nd + sqrt(self.grad_squ_sum_2nd[feat_id * self.k + f])) / self.alpha_2nd + self.beta_2nd)
                #vsum[f] += self.lazy_w_2nd[feat_id * self.k + f]
                #v2sum[f] += self.lazy_w_2nd[feat_id * self.k + f] ** 2
        
        #for f in range(self.k):
        #    z += .5 * (vsum[f] ** 2 - v2sum[f])

        #TODO: Compute per-factor sum of lazy_w_2nd
        for i in xrange(len(x)):
            for j in xrange(i + 1, len(x)):
                for f in xrange(self.k):
                    z += self.lazy_w_2nd[x[i] * self.k + f] * self.lazy_w_2nd[x[j] * self.k + f]

        return sigmoid(z)

    def read_sparse(self, path):
        """
        Apply hashing trick to the libsvm format sparse file. 

        Parameters:
        -----------
        path : string, point to libsvm format sparse file

        Returns:
        -----------
        An generator, like, (nozero_feature_idxes, y)
        """
        for line in open(path):
            xs = line.rstrip().split(' ')
            y = int(xs[0])
            nonzeros_feat_idxes = []
            for item in xs[1:]:
                fidx, _ = item.split(':')
                nonzeros_feat_idxes.append(int(fidx))
            yield nonzeros_feat_idxes, y

#==========================
# Util functions
#==========================
def logloss(y, pred):
    p = max(min(pred, 1.0 - 1e-15), 1e-15)
    return -log(p) if y == 1 else -log(1.0 - p)

def sigmoid(x):
    return 1.0 / (1.0 + exp(-max(min(x, 20.0), -20.0)))

#! /usr/bin/env python

import sys
from fm_ftrl_test import FTRLFM1
from metrics import auc, logloss, logloss2

if len(sys.argv) < 5:
    print "Usage: %s <train_file> <test_file> <result_file> <isstream" % sys.argv[0]
    exit(1)

train_file = sys.argv[1]
test_file = sys.argv[2]
result_file = sys.argv[3]
isstream = int(sys.argv[4])

best_auc = 0.0
best_loss = 0.0
best_dim = 0
for dim in [35]:
    clf = FTRLFM1(D=2**20, fm_dim=dim,
            alpha=0.1, beta=1.0, L1=0.001, L2=1,
            alpha_fm=0.1, beta_fm=1.0, L1_fm=0.001, L2_fm=1)

    n = 0
    batch_loss = 0.0
    batch_num = 1000000
    for x, y in clf.read_sparse(train_file):
        p = clf.predict(x) # predict for an input
        clf.update(x, p, y) # update the model with the target using error
        batch_loss += logloss2(y, p)
        n += 1
        if n % batch_num == 0:
            print "batch (%d) logloss=%f" % (n / batch_num, batch_loss/batch_num)
            batch_loss = 0 # reset

    true_labels = []
    preds = []

    with open(result_file, "w") as fwriter:
        for x, y in clf.read_sparse(test_file):
            p = clf.predict(x)
            true_labels.append(y)
            preds.append(p)
            if isstream:
                clf.update(x, p, y) # update the model with the target using error
            fwriter.write("%f %f\n" % (y, p))

    auc_val = auc(true_labels, preds)
    loss = logloss(true_labels, preds)
    
    if auc_val > best_auc:
        best_auc = auc_val
        best_loss= loss
        best_dim = dim
    print "FM dim = %d, AUC = %f, logloss=%f" % (dim, auc_val * 100, loss)


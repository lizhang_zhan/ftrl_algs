from __future__ import division
from collections import defaultdict
import numpy as np
from math import sqrt, exp

class FTRLLR(object):
    """
    Logistic Regression with FTRL online learner.

    Attributes:
    -----------
    n : int, number of features after hashing trick
    a : float, alpha in the per-coordinate rate
    b : float, beta in the per-coordinate rate
    l1 : float, l1 regularization parameter
    l2 : float, l2 regularization parameter
    w : a float array, feature weights
    c : a float array, counter for weights
    z : a float array, lazy weights
    interaction : whether to use 2nd order interaction or not
    """
    def __init__(self, n=2**20, a=.1, b=1.0, l1=1.0, l2=1.0, interaction=True):
        self.n = n
        self.alpha = a
        self.beta = b
        self.lambda1 = l1
        self.lambda2 = l2
        self.interaction = interaction

        # initialize weights and counts
        #self.w = np.zeros((self.n,), dtype=np.float64)
        #self.c = np.zeros((self.n), dtype=np.float64)
        #self.z = np.zeros((self.n,), dtype=np.float64)
        self.w = defaultdict(lambda:0.0)
        self.c = defaultdict(lambda:0.0)
        self.z = defaultdict(lambda:0.0)
    
    def _indices(self, x):
        for index in x:
            yield index

        if self.interaction:
            l = len(x)
            x = sorted(x)
            for i in xrange(l):
                for j in xrange(i + 1, l):
                    yield abs(hash('{feat1}_{feat2}'.format(feat1=x[i], feat2=x[j]))) % self.n
    
    def read_sparse(self, path):
        """
        Apply hashing trick to the libsvm format sparse file

        Parameters:
        -----------
            path : string,  a file path to the libsvm format sparse file

        Returns:
            x : list of int, a list of index of non-zero features
            y : int, target value
        """

        for line in open(path, 'r'):
            items = line.rstrip().split()
            y = int(items[0])
            x = []
            for item in items[1:]:
                index, _ = item.split(':')
                x.append(abs(hash(index)) % self.n)
            yield x, y
    
    def update(self, x, err):
        """
        Update the model with ftrl learner

        Parameters:
        ----------
        x : list of int, a list of non-zeros feature indexes
        err : err between prediction of the model and target

        Returns:
        ---------
            None
        """
        err2 = err * err

        for i in self._indices(x):
            s = (sqrt(self.c[i] + err2) - sqrt(self.c[i])) / self.alpha
            self.w[i] += err - s * self.z[i]
            self.c[i] += err2
    
    def predict(self, x):
        """
        Predict probability of instance `x` with postive label

        Parameters:
        -----------
            x : list of int, a list of index of non-zero features

        Returns:
        ----------
            p : float, a prediction for instance `x`
        """
        act = 0.0
        for i in self._indices(x):
            sign = -1.0 if self.w[i] < 0 else 1
            if sign * self.w[i] <= self.lambda1:
                self.z[i] = 0.0
            else:
                self.z[i] = (sign * self.lambda1 - self.w[i]) / \
                        ((self.beta + sqrt(self.c[i])) / self.alpha + self.lambda2)
            act += self.z[i]

        return sigmoid(act)

#==========================
# Util functions
#==========================
def logloss(y, pred):
    p = max(min(pred, 1.0 - 1e-15), 1e-15)
    return -log(p) if y == 1 else -log(1.0 - pred)

def sigmoid(x):
    return 1.0 / (1.0 + exp(-max(min(x, 20.0), -20.0)))


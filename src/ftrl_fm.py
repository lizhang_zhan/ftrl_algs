# encoding: utf-8
from __future__ import division
from collections import defaultdict
from math import exp, copysign, log, sqrt
from datetime import datetime
import random

class FTRLFM(object):
    def __init__(self, D = 2**20, L=10, alpha=0.15, beta=1.0, l1 = 1.0, l2=1.0,
                alpha_2nd = 0.15, beta_2nd=1.0, l1_2nd = 1.0, l2_2nd=1.0,
                mu = 0.0, sigma = 0.1, dropout_rate = 1.0):
        self.alpha = alpha
        self.beta = beta
        self.l1 = l1
        self.l2 = l2
        self.alpha_2nd = alpha_2nd
        self.beta_2nd = beta_2nd
        self.l1_2nd = l1_2nd
        self.l2_2nd = l2_2nd
        self.mu = mu
        self.sigma = sigma
        self.dropout_rate = dropout_rate

        self.D = D
        self.L = L

        # model parameters
        self.n = [0.0] * (D + 1)
        self.z = [0.0] * (D + 1)
        self.w = [0.0] * (D + 1)

        self.n_2nd = defaultdict(lambda:[0.0] * self.L)
        self.z_2nd = defaultdict(lambda: gauss_random(L, mu, sigma))
        self.w_2nd = defaultdict(lambda: [0.0] * self.L)

    def predict(self, x):
        weight_sum = 0
        # bias contribution
        #self.w[0] = -(self.z[0] / ((self.beta + sqrt(self.n[0])) / self.alpha))
        #weight_sum += self.w[0]

        # calculate the first order contribution
        for fid in [0] + x:
            sign = -1.0 if self.z[fid] < 0.0 else 1.0
            if sign * self.z[fid] <= self.l1:
                self.w[fid] = 0.0
            else:
                self.w[fid] = -(self.z[fid] - sign * self.l1) / ((self.beta + sqrt(self.n[fid])) / self.alpha + self.l2)
            weight_sum += self.w[fid]
        
        sub_sum = [0.0] * self.L
        sub_2sum = [0.0] * self.L
        for fid in x:
            for k in xrange(self.L):
                sign = -1.0 if self.z_2nd[fid][k] < 0.0 else 1.0

                if sign * self.z_2nd[fid][k] <= self.l1_2nd:
                    self.w_2nd[fid][k] = 0.0
                else:
                    self.w_2nd[fid][k] = -(self.z_2nd[fid][k] - sign * self.l1_2nd) / ((self.beta_2nd + sqrt(self.n_2nd[fid][k])) / self.alpha_2nd + self.l2_2nd)
                sub_sum[k] += self.w_2nd[fid][k]
                sub_2sum[k] += self.w_2nd[fid][k] ** 2

        # calculate the second order contribution
        for k in xrange(self.L):
            weight_sum += (sub_sum[k] ** 2 - sub_2sum[k]) * 0.5

        return sigmoid(weight_sum)

    def update(self, x, err):
        # cost gradient
        grad = err

        # update the first order weight 
        for fid in [0] + x: # include bias 
            sigma = (sqrt(self.n[fid] + grad * grad) - sqrt(self.n[fid])) / self.alpha
            self.z[fid] = grad - sigma * self.w[fid]
            self.n[fid] += grad * grad
        
        latent_weight_sum = [0.0] * self.L
        for k in xrange(self.L):
            for fid in x:
                latent_weight_sum[k] += self.w_2nd[fid][k]

        for fid in x:
            for k in xrange(self.L):
                grad_2nd = grad * (latent_weight_sum[k] - self.w_2nd[fid][k])
                sigma = (sqrt(self.n_2nd[fid][k] + grad_2nd * grad_2nd) - sqrt(self.n_2nd[fid][k])) / self.alpha_2nd
                self.z_2nd[fid][k] += grad_2nd - sigma * self.w_2nd[fid][k]
                self.n_2nd[fid][k] += grad_2nd * grad_2nd
    
    def dump_model(model_file):
        with open(model_file, "w") as fp:
            fp.write("%d"%self.D)
            fp.write("%d"%self.L)
            for i, w in enumerate(self.w):
                fp.write("{dim},{weight}\n".format(dim=i, weight=w))
            for i, weights in self.w_2nd.iteritems():
                fp.write("%i, %s\n" % (i, ",".join([str(w) for w in weights])))
    
    def read_sparse(self, path):
        for line in open(path):
            items = line.rstrip().split()
            label = int(items[0])
            feat_idxes = []
            for item in items[1:]:
                fid, _ = item.split(':')
                feat_idxes.append(int(fid))
            yield feat_idxes, label
    
    def gauss_random(self):
        ret = []
        for i in xrange(self.L):
            ret.append(random.gauss(self.mu, self.sigma))
        return ret 


#===================
# Util functions
#===================
def gauss_random(L, mu, sigma):
    ret = []
    for i in xrange(L):
        ret.append(random.gauss(mu, sigma))
    return ret 

def sigmoid(z):
    z_ = max(min(z, 35.0), -35.0)
    return 1.0 / (1.0 + exp(-z_))

def logloss(y, pred):
    p = max(min(p, 1.0 - 1e-15), 1e-15)
    return -log(p) if y == 1 else -log(1.0 - p)

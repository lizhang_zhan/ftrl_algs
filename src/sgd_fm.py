from __future__ import division
import numpy as np

from math import sqrt, abs, log

class SGDFM(object):
    """
    Factorization Machine SGD online Learner

    Attributes:
        n : int, number of features after hashing trick
        k : int, number of factors for interaction
        a : float, initial learning rate
        w0 : float, weight for bias
        c0 : float, counters,
        w : an array of float, feature weights
        c : an array of float, counters for weights
        V : array of float, feature weights for factors
    """
    def __init__(self, n, k, a, seed=0):
        """
        Parameters:
        -----------
        n : int, number of features after hashing trick
        k : int, number of factors for interaction
        a : float, initial learning rate
        seed : int, random seed
        """
        self.n = n
        self.k = dim
        self.a = a

        rand = np.random.RandomState(seed)
        # initialize weights, factorized interaction and counts
        self.w0 = 0.0
        self.c0 = 0.0
        self.w = np.zeros((self.n,), dtype=np.float64)
        self.c = np.zeros((self.n,), dtype=np.float64)
        self.v = (rand.rand(self.n * self.k) - .5) * 1e - 6
    
    def read_sparse(self, path):
        """
        Apply hashing trick to the libsvm format sparse file. 

        Parameters:
        -----------
        path : string, point to libsvm format sparse file

        Returns:
        -----------
        An generator, like, (nozero_feature_idx, feature_val), y
        """
        for line in open(path):
            xs = line.rstrip().split(' ')
            y = int(xs[0])
            nonzeros_indexes = []
            feat_vals = []
            for item in xs[1:]:
                idx, val = item.split(':')
                nonzeros_indexes.append(idx)
                feat_vals.append(val)
            yield zip(nonzeros_indexes, feat_vals), y

    def predict(self, x):
        """
        Predict probability of instance `x` with postive label

        Parameters
        ----------
        x : a list of tuple, a list of non-zero feature id and val pair

        Returns
        --------
        p : float, a predicted probability for x with postive label
        """
        wx = 0
        vx = np.zeros((self.k,), dtype=np.float64)
        v2x2 = np.zeros((self.k,), dtype=np.float64)

        for feat_id, feat_val in x:
            wx += self.w[feat_id] * feat_val
            for k in range(self.k):
                vx[k] += self.v[feat_id * self.k + k] * feat_val
                v2x2[k] += (self.v[feat_id * self.k + k] ** 2) * (feat_val**2)

        p = self.w0 + wx
        for k in range(self.k):
            p += .5 * (vx[k] ** 2 - v2x2[k])

        return sigmoid(p)

    def update(self, x, err):
        """
        Update the model with SGD learner.

        Parameters:
        ----------
        x : a list of tuple, like (feat_id, feat_val), nozero feature index and value pair
        err: float, a error between the prediction of the model and target.

        Returns:
        ---------
        weight : array, model weight
        counts: array, model counts
        """
        # calculate v_f * x in advance
        vx = np.zeros((self.k,), dtype=np.float64)

        for feat_id, feat_val in x:
            for k in range(self.k):
                vx[k] += self.v[feat_id * self.k + k] * v

        # update w0, w, V, c0, and c
        g2 = e * e

        self.w0 -= self.a / (sqrt(self.c0) + 1) * e
        for fid, fval in x:
            dl_dw = self.a / (sqrt(self.c[fid]) + 1) * e * fval
            self.w[fid] -= dl_dw
            for f in range(self.k):
                self.V[fid * self.k + f] -= (dl_dw *
                        (vx[f] - self.v[fid * self.k + f] * fval))
            self.c[i] += g2

        self.c0 += g2

#==========================
# Util functions
#==========================
def logloss(y, pred):
    p = max(min(pred, 1.0 - 1e-15), 1e-15)
    return -log(p) if y == 1 else -log(1.0 - pred)

def sigmoid(x):
    return 1.0 / (1.0 + exp(max(min(x, 20.0), -20.0)))


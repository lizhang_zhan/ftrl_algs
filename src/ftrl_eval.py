#! /usr/bin/env python

import sys
from ftrl_lr import FTRLLR
from metrics import auc, logloss, logloss2

if len(sys.argv) < 6:
    print "Usage: %s <train_file> <test_file> <result_file> <interaction> <isstream>" % sys.argv[0]
    exit(1)

train_file = sys.argv[1]
test_file = sys.argv[2]
result_file = sys.argv[3]
interaction = int(sys.argv[4])
isstream = int(sys.argv[5])

clf = FTRLLR(n=2**24, # number of hashed features
        a=.1, # alpha in the per-coordinate rate
        b=1, # beta in the per-coordinate rate
        l1=1., # L1 regularization parameter
        l2=1., # L2 regularization parameter
        interaction=interaction) # use feature interaction or no

n = 0
batch_loss = 0
batch_num = 1000000
for x, y in clf.read_sparse(train_file):
    p = clf.predict(x) # predict for an input
    clf.update(x, p - y) # update the model with the target using error
    n += 1
    batch_loss += logloss2(y, p)
    if n % batch_num == 0:
        print "Batch (%d, %d) logloss=%f" % (n / batch_num, n, batch_loss / batch_num)
        batch_loss = 0

true_labels = []
preds = []

with open(result_file, "w") as fwriter:
    for x, y in clf.read_sparse(test_file):
        p = clf.predict(x)
        true_labels.append(y)
        preds.append(p)
        if isstream:
            clf.update(x, p - y)
        fwriter.write("%f %f\n" % (y, p))

auc_val = auc(true_labels, preds)
loss = logloss(true_labels, preds)

print "ftrl AUC = %f, logloss=%f" % (auc_val * 100, loss)

#! /usr/bin/env python
import sys
import numpy as np
import matplotlib.pyplot as plt

from sklearn.isotonic import IsotonicRegression as IR
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score as AUC

from metrics import logloss
from util import get_bin_hist
from util import read_result

def split_train_test(ylabels, preds, rate=0.5):
    """
    Returns:
    --------
    x_train, x_test, y_train, y_test
    """
    split_idx = ylabels.shape[0] / 2
    return (preds[:split_idx], preds[split_idx+1:],
            ylabels[:split_idx], ylabels[split_idx+1:])

def main(result_file):
    ylabels, preds = read_result(result_file)
    x_train, x_test, y_train, y_test = split_train_test(ylabels, preds)

    # platts scaling with lr
    ir = IR(out_of_bounds = 'clip')
    ir.fit(x_train, y_train)
    p_calibrated = ir.transform(x_test)
    p_calibrated[np.isnan(p_calibrated)] = 0 # fix NAN val
    
    # accuracy comparasion
    acc = accuracy_score(y_test, np.round(x_test))
    acc_calibrated = accuracy_score(y_test, np.round(p_calibrated))
    print "acc - before / after: ", acc, "/", acc_calibrated
    
    # auc comparasion
    auc  = AUC(y_test, x_test)
    auc_calibrated = AUC(y_test, p_calibrated)
    print "auc - before / after: ", auc, "/", auc_calibrated
    
    # logloss
    ll = logloss(y_test, x_test)
    ll_calibrated = logloss(y_test, p_calibrated)
    print "logloss - before / after: ", ll, "/", ll_calibrated
    
    fig = plt.figure()
    label_bins, pred_bins = get_bin_hist(y_test, x_test)
    plt.plot(label_bins, pred_bins, label='no-calibrate')

    # calibrated
    label_bins, pred_bins = get_bin_hist(y_test, p_calibrated)
    plt.plot(label_bins, pred_bins, 'red', label='calibrated')
   
    # perfect calibration line
    plt.plot(np.linspace(0, 1), np.linspace(0, 1), 'gray', label='perfect')
    
    plt.legend(('no-calibrate', 'calibrated', 'perfect'), loc='lower right')

    plt.title('Isotonic regression')

    plt.show()
if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage: %s <result_file>" % sys.argv[0]
        exit(1)
    main(sys.argv[1])

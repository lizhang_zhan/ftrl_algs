#! /usr/bin/env python
# encoding: utf-8

import sys
import numpy as np
import matplotlib.pyplot as plt

from  sklearn.linear_model import LogisticRegression as LR
from sklearn.metrics import accuracy_score
#from sklearn.metrics import roc_auc_score as AUC
from metrics import auc as AUC
from metrics import logloss
from util import get_bin_hist, read_result

def split_train_test(ylabels, preds, rate=0.5):
    """
    parameters:
    ylabels : array, a list of target labels, 0 or 1.
    preds : array, a list of predicted probability, [0, 1]

    Returns:
    --------
    x_train, x_test, y_train, y_test
    """
    split_idx = ylabels.shape[0] / 2
    return (preds[:split_idx].reshape(-1, 1), preds[split_idx+1:].reshape(-1, 1),
            ylabels[:split_idx], ylabels[split_idx+1:])

def main(result_file):
    ylabels, preds = read_result(result_file)
    x_train, x_test, y_train, y_test = split_train_test(ylabels, preds)

    # platts scaling with lr
    lr = LR(max_iter=1000)
    lr.fit(x_train, y_train)
    p_calibrated = lr.predict_proba(x_test)[:,1]

    acc = accuracy_score(y_test, np.round(x_test))
    acc_calibrated = accuracy_score(y_test, np.round(p_calibrated))
    print "acc - before / after: ", acc, "/", acc_calibrated

    auc  = AUC(y_test, x_test)
    auc_calibrated = AUC(y_test, p_calibrated)
    print "auc - before / after: ", auc, "/", auc_calibrated
    
    ll = logloss(y_test, x_test)
    ll_calibrated = logloss(y_test, p_calibrated)
    print "logloss - before / after: ", ll, "/", ll_calibrated
    
if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage: %s <result_file>" % sys.argv[0]
        exit(1)
    main(sys.argv[1])

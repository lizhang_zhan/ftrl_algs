#! /usr/bin/env python
# encoding: utf-8

import sys
import numpy as np
import matplotlib.pyplot as plt
from util import get_bin_hist, read_result

def plot_reliability(ylabels, preds, nbins=50):
    true_bins, pred_bins = get_bin_hist(ylabels, preds, nbins)
    plt.plot(pred_bins, true_bins)
    plt.plot(np.linspace(0, 1), np.linspace(0, 1), 'gray')
    plt.show()


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Usage: %s <result_file>" % sys.argv[0]
    result_file = sys.argv[1]
    ylabels, preds = read_result(result_file)
    plot_reliability(ylabels, preds)

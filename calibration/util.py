import numpy as np

def get_bin_hist(ylabels, preds, n_bins=50):
    n_bins = float(n_bins)

    pred_bins = np.empty((0,))
    true_bins = np.zeros((0,))

    for bin in range(1, int(n_bins) + 1):
        idxes = np.logical_and(preds <= bin / n_bins, preds > (bin - 1) / n_bins)
        if np.sum(idxes) == 0:
            continue

        pred_bin = np.mean(preds[idxes])
        true_bin = np.sum(ylabels[idxes] / np.sum(idxes))
        # print pred_bin, true_bin
        pred_bins = np.hstack((pred_bins, pred_bin))
        true_bins = np.hstack((true_bins, true_bin))

    return (true_bins, pred_bins)


def read_result(file_path):
    fmt = lambda t: (float(t[0]), float(t[1]))
    ylabels, preds = [], []
    with open(file_path, 'r') as reader:
        for line in reader:
            y, p = fmt(line.split())
            ylabels.append(y)
            preds.append(p)
    return np.asarray(ylabels, dtype=np.float32), np.asarray(preds, dtype=np.float32)
